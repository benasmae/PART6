<html>
    <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, intial-scale=1.0"/>
    <title>Creat New</title>  
            <title>Webslesson Tutorial | Append Data to JSON File using PHP</title>  
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>  
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />  
            <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>             
</head>
    <body>
    <div class="msg">
</div>
   <div class="container" style="width:500px;">  
                 <h3>Append Data to JSON File</h3><br /> 
            <form method="POST">
                <div>              
                <label>Name</label>  
                      <input type="text" name="name" class="form-control" /><br />  
                      <label>Gender</label>  
                      <input type="file" name="photo" class="form-control" /><br />  
                      <label>Designation</label>  
                      <input type="text" name="characteristics" class="form-control" /><br />  
                    <button type="submit" class="btn btn-primary" name="create">Add Character</button>
                </div>
            </form>
            <h4>This is JSON-VERSION</h4>
        </div>  
    </div>
    </body>
    </html>
    <?php
  
    
    if(isset($_POST['create'])){
 
        $data = file_get_contents('data.json');
        $data_array = json_decode($data);
        $file_path = "images/".$_POST['photo'];     
 
        $input = array(
         
            'name' => $_POST['name'],
            'photo' => $file_path,
            'characteristics' => $_POST['characteristics'],
            
        );
 
        $data_array[] = $input;
        $data_array = json_encode($data_array, JSON_PRETTY_PRINT);
        unlink('data.json');
         file_put_contents('data.json', $data_array);
        $data = file_get_contents('data.json');
        
        $_SESSION['message'] = 'Data successfully appended';
    }
    else{
        $_SESSION['message'] = 'Fill up add form first';
    }
    
?>