<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <title>cube effect</title>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">

  <link rel="stylesheet" href="style.css">

  <link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.css" />
<link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.min.css" />

<script src="https://unpkg.com/swiper/swiper-bundle.js"></script>
<script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>

  
   
  
</head>

<body>

<a href="ajouterper.php" class="ajouterper">add character</a>
    <div class="swiper-container">

        <div class="swiper-wrapper">

          
           
                   <?php   
                          $data = file_get_contents("data.json");  
                          $data = json_decode($data, true);  
                          foreach($data as $row)  
                          {    
                            ?>
                            <div class="swiper-slide">
                            <div class="img-box"><img  src="<?php echo $row["photo"];?>"/></div>
                            <div class="content"><h3><?php echo $row["name"];?><br><span><?php echo $row["characteristics"];?></h3></span></div>
                          </div> <?php 
                          }  
                          ?> 

       
          
    </div>
    
      </div>
      <!-- Add Pagination -->
    <div class="swiper-pagination"> </div>

      </div>

      

      <script type="text/javascript">

           var swiper = new Swiper('.swiper-container', {
               effect: 'cube',
               grabCursor: true,
               cubeEffect: {
               shadow: true,
               slideShadows: true,
               shadowOffset: 20,
               shadowScale: 0.94,
                                 
              },

              pagination: {
              el: '.swiper-pagination',
               },

        
            });

      
       </script>
       </body>

       </html>